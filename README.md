# hyssop
## a portage overlay


hyssop (n): a European mint *(Hyssopus officinalis)* with highly aromatic and pungent leaves

## overview

this is my personal ebuild repository (or overlay) for use with portage

- most ebuilds here are live ebuilds
- this repository will slowly grow over time

## installation

enable using eselect-repository

```
eselect repository add hyssop git https://codeberg.org/nya/hyssop-overlay
emerge --sync hyssop
```

## license
GNU GPLv2

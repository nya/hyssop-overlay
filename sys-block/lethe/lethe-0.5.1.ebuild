# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cargo

CRATES="
	CoreFoundation-sys-0.1.4
	IOKit-sys-0.1.5
	adler32-1.0.4
	aho-corasick-0.7.10
	ansi_term-0.11.0
	anyhow-1.0.28
	arrayref-0.3.6
	arrayvec-0.5.1
	assert_matches-1.3.0
	atty-0.2.14
	autocfg-1.0.0
	base64-0.11.0
	bitflags-1.2.1
	blake2b_simd-0.5.10
	bstr-0.2.12
	byteorder-1.3.4
	cc-1.0.52
	cfg-if-0.1.10
	clap-2.33.0
	clicolors-control-1.0.1
	cloudabi-0.0.3
	console-0.11.2
	console-0.7.7
	constant_time_eq-0.1.5
	crc32fast-1.2.0
	crossbeam-utils-0.7.2
	csv-1.1.3
	csv-core-0.1.10
	dirs-1.0.5
	encode_unicode-0.3.6
	flate2-1.0.14
	getrandom-0.1.14
	hermit-abi-0.1.12
	indicatif-0.11.0
	itoa-0.4.5
	lazy_static-1.4.0
	libc-0.2.69
	lock_api-0.3.4
	mach-0.1.2
	memchr-2.3.3
	miniz_oxide-0.3.6
	nix-0.14.1
	num-traits-0.2.11
	number_prefix-0.2.8
	numtoa-0.2.3
	parking_lot-0.10.2
	parking_lot_core-0.7.2
	ppv-lite86-0.2.6
	prettytable-rs-0.8.0
	proc-macro2-1.0.24
	quote-1.0.8
	rand-0.7.3
	rand_chacha-0.2.2
	rand_core-0.5.1
	rand_hc-0.2.0
	redox_syscall-0.1.56
	redox_users-0.3.4
	regex-1.3.7
	regex-automata-0.1.9
	regex-syntax-0.6.17
	roaring-0.6.2
	rust-argon2-0.7.0
	ryu-1.0.4
	scopeguard-1.1.0
	serde-1.0.106
	smallvec-1.4.0
	streaming-iterator-0.1.5
	strsim-0.8.0
	syn-1.0.56
	sysfs-class-0.1.3
	term-0.5.2
	terminal_size-0.1.12
	termios-0.3.2
	textwrap-0.11.0
	thiserror-1.0.22
	thiserror-impl-1.0.22
	thread_local-1.0.1
	unicode-width-0.1.7
	unicode-xid-0.2.1
	vec_map-0.8.1
	void-1.0.2
	wasi-0.9.0+wasi-snapshot-preview1
	widestring-0.4.0
	winapi-0.3.8
	winapi-i686-pc-windows-gnu-0.4.0
	winapi-util-0.1.5
	winapi-x86_64-pc-windows-gnu-0.4.0
"

DESCRIPTION="A secure, free, cross-platform and open-source drive wiping utility"
HOMEPAGE="https://github.com/Kostassoid/lethe"
SRC_URI="https://github.com/Kostassoid/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
	$(cargo_crate_uris ${CRATES})"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

# Copyright 1999-2021 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="lesbar is a scriptable status bar for X11, heavily inspired by lemonbar"
HOMEPAGE="https://git.sr.ht/~salmiak/lesbar"
SRC_URI="https://git.sr.ht/~salmiak/lesbar/archive/v${PV}.tar.gz -> ${PN}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64"

DEPEND="${BDEPEND}"
BDEPEND="
	app-text/scdoc
	x11-libs/cairo
	dev-libs/glib
	x11-libs/pango
	x11-libs/libX11
	dev-util/pkgconfig
	dev-libs/libffi
"

S="${WORKDIR}/${PN}-v${PV}"

pkg_setup(){
	export PREFIX="/usr"
}


# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit font

DESCRIPTION="a free and open-source pixel grid-based monospace typeface suitable for programming, writing and everyday use"
HOMEPAGE="https://github.com/andirueckel/undefined-medium"
SRC_URI="https://github.com/andirueckel/undefined-medium/archive/refs/tags/v${PV}.tar.gz -> ${PN}.tar.gz"

LICENSE="OFL-1.1"
SLOT="0"
KEYWORDS="amd64"

S="${WORKDIR}/${PN}-${PV}"
FONT_S=( "${S}/fonts/ttf" "${S}/fonts/otf" )

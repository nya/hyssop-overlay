# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit font font-ebdftopcf

DESCRIPTION="A small bitmap font"
HOMEPAGE="https://github.com/lucy/tewi-font"
SRC_URI="https://github.com/lucy/tewi-font/archive/${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="amd64"

LICENSE="MIT"
SLOT="0"
IUSE=""
BDEPEND="
	x11-apps/bdftopcf
"

FONT_S=( ${S} ${S}/variant )

src_compile() {
        font-ebdftopcf_src_compile
}

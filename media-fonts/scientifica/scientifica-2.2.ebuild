# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit font font-ebdftopcf

DESCRIPTION="Tall and condensed bitmap font for geeks."
HOMEPAGE="https://github.com/NerdyPepper/scientifica"
SRC_URI="https://github.com/NerdyPepper/scientifica/releases/download/v${PV}/${PN}.tar"
KEYWORDS="~amd64"

LICENSE="MIT"
SLOT="0"
IUSE=""
BDEPEND="
	x11-apps/bdftopcf
"

S="${WORKDIR}"
FONT_S=( ${S}/${PN}/bdf )
FONTDIR="/usr/share/fonts"

src_compile() {
        font-ebdftopcf_src_compile
}

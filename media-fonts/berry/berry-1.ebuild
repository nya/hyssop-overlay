# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit font git-r3

DESCRIPTION="A set of monospaced bitmap fonts made by Spectrum"
HOMEPAGE="https://github.com/seraxis/pcf-spectrum-berry"
EGIT_REPO_URI="https://github.com/seraxis/pcf-spectrum-berry"

LICENSE="unknown"
SLOT="0"
KEYWORDS="amd64"

S="${WORKDIR}/${P}"
FONT_S=( ${S} )
FONT_SUFFIX="pcf"
